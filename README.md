![N|Solid](logo_ioasys.png)

# README #

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto Empresas.

### O QUE FAZER ? ###

* Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso.

### ESCOPO DO PROJETO ###

* Deve ser criado um aplicativo Android utilizando linguagem Java ou Kotlin com as seguintes especificações:
* Login e acesso de Usuário já registrado
	* Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers (access-token, client, uid);
	* Para ter acesso as demais APIS precisamos enviar esses 3 custom headers para a API autorizar a requisição;
* Listagem de Empresas
* Detalhamento de Empresas

### Informações Importantes ###

* Layout e recortes disponíveis no Zeplin (http://zeplin.io)
Login - teste_ioasys
Senha - ioasys123

* Integração disponível a partir de uma collection para Postman (https://www.getpostman.com/apps) disponível neste repositório.
* O `README.md` deve conter uma pequena justificativa de cada biblioteca adicionada ao projeto como dependência.
* O `README.md` deve conter tambem o que você faria se tivesse mais tempo.
* O `README.md` do projeto deve conter instruções de como executar a aplicação
* Independente de onde conseguiu chegar no teste é importante disponibilizar seu fonte para analisarmos.

### Dados para Teste ###

* Servidor: https://empresas.ioasys.com.br
* Versão da API: v1
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234

### Dicas ###

* Para requisição sugerimos usar a biblioteca Retrofit
* Para download e cache de imagens use a biblioteca Glide
* Para parse de Json use a biblioteca GSON

### Bônus ###

* Testes unitários, pode usar a ferramenta que você tem mais experiência, só nos explique o que ele tem de bom.
* Usar uma arquitetura testável. Ex: MVP, MVVM, Clean, etc.
* Material Design
* Utilizar alguma ferramenta de Injeção de Dependência, Dagger, Koin e etc..
* Utilizar Rx, LiveData, Coroutines.
* Padrões de projetos

### Justificativa das Bibliotecas usadas: ###

* Para testes unitários e instrumentados:
    androidTestImplementation 'androidx.test.ext:junit-ktx:1.1.1'
    androidTestImplementation 'androidx.test:runner:1.2.0'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.2.0'
    testImplementation 'junit:junit:4.12'
    testImplementation 'androidx.test:core:1.2.0'
    testImplementation 'androidx.test.ext:junit:1.1.1'
    testImplementation 'androidx.test.ext:truth:1.2.0'
    testImplementation 'androidx.test.espresso:espresso-core:3.2.0'
    testImplementation 'androidx.test.espresso:espresso-intents:3.2.0'
    testImplementation 'org.robolectric:robolectric:4.3'

* Para RecyclerViewActions:
    testImplementation 'androidx.test.espresso:espresso-contrib:3.2.0'
    androidTestImplementation 'androidx.test.espresso:espresso-contrib:3.2.0'

* Para método launchActivity() de ActivityScenario:
    androidTestImplementation 'androidx.test:core-ktx:1.2.0'

* Para consumo de API:
    implementation 'com.squareup.retrofit2:retrofit:2.4.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.3.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:3.4.0'
    implementation 'com.squareup.okhttp3:okhttp:3.11.0'

* Para RecyclerView (lista):
    implementation 'androidx.recyclerview:recyclerview:1.1.0-beta04'

* Para Picasso (cache e exibição de imagens):
    implementation 'com.squareup.picasso:picasso:2.5.2'

* Para fragments (AndroidX):
    implementation 'androidx.fragment:fragment:1.2.0-alpha04'
    implementation 'androidx.fragment:fragment-ktx:1.2.0-alpha04'

* Para Dagger 2 (injeção de dependências):
    implementation 'com.google.dagger:dagger:2.24'
    kapt 'com.google.dagger : dagger-compiler:2.24'
    implementation 'com.google.dagger:dagger-android:2.24'
    implementation 'com.google.dagger:dagger-android-support:2.24'
    kapt 'com.google.dagger:dagger-android-processor:2.24'

* Para views de Material Design:
    implementation 'com.google.android.material:material:1.0.0'

* Para RxKotlin:
    implementation 'io.reactivex.rxjava2:rxkotlin:2.4.0'
    implementation 'io.reactivex.rxjava2:rxandroid:2.1.1'

* Para RxRetrofit:
    implementation 'com.squareup.retrofit2:adapter-rxjava2:2.4.0'

* Para ARC (Android Archtecture Components):
    implementation 'android.arch.lifecycle:extensions:1.1.1'
    kapt "android.arch.lifecycle:compiler:1.1.1"

* Para Room (banco de dados):
    implementation 'android.arch.persistence.room:runtime:1.1.1'
    implementation 'android.arch.persistence.room:rxjava2:1.1.1'
    kapt "android.arch.persistence.room:compiler:1.1.1"

* Para coroutines:
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.2'

* Para erro de SSL: (https://stackoverflow.com/questions/29916962/javax-net-ssl-sslhandshakeexception-javax-net-ssl-sslprotocolexception-ssl-han)
    implementation 'com.google.android.gms:play-services-auth:17.0.0'

* Para obter os headers (O Retrofit não retorna todos os headers no response):
    implementation 'com.android.volley:volley:1.1.1'

### O que eu faria caso tivesse mais tempo: ###

* Uma tela de Configurações em que o usuário poderia escolher manter a sessão de login ativa ou deslogar-se;
* Uso de coroutines nessa tela de Configurações, obtendo uma lista de data e hora em que o usuário logou na plataforma (todos esses dados seriam salvos e obtidos do banco de dados).

### Como executar a aplicação: ###

* Permita, através das configurações do Android, que o dispositivo instale aplicativos de "Fontes Desconhecidas" ou que não necessite ser analisado pelo Play Protect;
* Transfira o arquivo .apk da pasta "apk" do projeto para o seu dispositivo;
* Na pasta de arquivos do dispositivo, ache o arquivo .apk e execute-o;
* Na tela de apresentação das permissões necessárias que o aplicativo necessita, clique em "Instalar";
* Inicie o aplicativo a partir do menu de aplicações do Android, clicando em seu ícone.