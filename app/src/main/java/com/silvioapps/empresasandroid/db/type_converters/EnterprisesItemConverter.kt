package com.silvioapps.empresasandroid.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.empresasandroid.features.list.models.EnterprisesItem

class EnterprisesItemConverter{

    @TypeConverter
    fun from(value: List<EnterprisesItem>): String{
        val gson = Gson()
        val type = object: TypeToken<List<EnterprisesItem>>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): List<EnterprisesItem>{
        val gson = Gson()
        val type = object: TypeToken<List<EnterprisesItem>>(){}.type
        return gson.fromJson(value, type)
    }
}