package com.silvioapps.empresasandroid.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.empresasandroid.features.list.models.EnterpriseType

class EnterpriseTypeConverter{

    @TypeConverter
    fun from(value: EnterpriseType): String{
        val gson = Gson()
        val type = object: TypeToken<EnterpriseType>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): EnterpriseType{
        val gson = Gson()
        val type = object: TypeToken<EnterpriseType>(){}.type
        return gson.fromJson(value, type)
    }
}