package com.silvioapps.empresasandroid.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.empresasandroid.features.login.models.Portfolio

class PortfolioConverter{

    @TypeConverter
    fun from(value: Portfolio): String{
        val gson = Gson()
        val type = object: TypeToken<Portfolio>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): Portfolio{
        val gson = Gson()
        val type = object: TypeToken<Portfolio>(){}.type
        return gson.fromJson(value, type)
    }
}