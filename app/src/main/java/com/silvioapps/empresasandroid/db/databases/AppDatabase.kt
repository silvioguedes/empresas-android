package com.silvioapps.empresasandroid.db.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.silvioapps.empresasandroid.features.list.daos.ListDao
import com.silvioapps.empresasandroid.features.login.daos.LoginDao
import com.silvioapps.empresasandroid.db.type_converters.*
import com.silvioapps.empresasandroid.features.list.models.*
import com.silvioapps.empresasandroid.features.login.models.Portfolio
import com.silvioapps.empresasandroid.features.login.models.LoginResponse as LoginResponse

@TypeConverters(StringConverter::class, EnterprisesItemConverter::class,
    PortfolioConverter::class, EnterpriseTypeConverter::class)
@Database(entities = [ListResponse::class, EnterprisesItem::class, Portfolio::class,
    LoginResponse::class, EnterpriseType::class],
    version = 4, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun listDao(): ListDao
    abstract fun loginDao(): LoginDao
}