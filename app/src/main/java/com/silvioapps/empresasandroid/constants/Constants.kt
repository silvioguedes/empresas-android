package com.silvioapps.empresasandroid.constants

class Constants {
    companion object {
        const val API_BASE_URL = "https://empresas.ioasys.com.br/api/v1/"
        const val LOGIN = "users/auth/sign_in"
        const val LIST = "enterprises"
        const val API_TIMEOUT : Long = 15
        const val RATE_TIMEOUT : Long = 10
        const val ITEMS_OFFSET : Int = 10
    }
}
