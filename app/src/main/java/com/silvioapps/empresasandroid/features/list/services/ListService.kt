package com.silvioapps.empresasandroid.features.list.services

import androidx.lifecycle.LiveData
import com.silvioapps.empresasandroid.features.shared.arc.ApiResponse
import com.silvioapps.empresasandroid.constants.Constants
import com.silvioapps.empresasandroid.features.list.models.ListResponse
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ListService {
    @GET(Constants.LIST)
    fun getLiveDataApiResponse(@Header("access-token") accessToken: String,
                               @Header("client") client: String,
                               @Header("uid") uid: String,
                               @Query("name") name: String): LiveData<ApiResponse<ListResponse>>
}