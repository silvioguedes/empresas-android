package com.silvioapps.empresasandroid.features.login.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class Portfolio(
	@PrimaryKey(autoGenerate = true)
	var id: Int? = null,

	@field:SerializedName("enterprises_number")
	val enterprisesNumber: Int? = null

	//@field:SerializedName("enterprises")
	//val enterprises: List<Any?>? = null
): Serializable