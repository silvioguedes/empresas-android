package com.silvioapps.empresasandroid.features.list.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.silvioapps.empresasandroid.features.shared.arc.Resource
import com.silvioapps.empresasandroid.features.list.repositories.ListRepository
import com.silvioapps.empresasandroid.features.list.models.EnterprisesItem
import javax.inject.Inject;

class ListViewModel @Inject constructor(private var repository: ListRepository): ViewModel() {

    fun getLiveDataResorceEnterprisesItemList(accessToken: String, client: String, uid: String, search: String): LiveData<Resource<List<EnterprisesItem>>> {
        return repository.getLiveDataResorceEnterprisesItemList(accessToken, client, uid, search)
    }
}