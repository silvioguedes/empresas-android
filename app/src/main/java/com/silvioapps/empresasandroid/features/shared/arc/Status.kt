package com.silvioapps.empresasandroid.features.shared.arc

enum class Status {
    UNKNOWN_CODE,
    SUCCESS,
    ERROR,
    LOADING
}