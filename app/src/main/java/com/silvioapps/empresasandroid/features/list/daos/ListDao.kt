package com.silvioapps.empresasandroid.features.list.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.silvioapps.empresasandroid.features.list.models.EnterprisesItem

@Dao
abstract class ListDao{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun setEnterprisesItemList(value: List<EnterprisesItem>)

    @Query("SELECT * FROM EnterprisesItem WHERE enterpriseName LIKE :search")
    abstract fun getLiveDataEnterprisesItemList(search: String): LiveData<List<EnterprisesItem>>
}