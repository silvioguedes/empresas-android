package com.silvioapps.empresasandroid.features.list.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class ListResponse(
	@PrimaryKey(autoGenerate = true)
	var id: Int? = null,

	@field:SerializedName("enterprises")
	val enterprises: List<EnterprisesItem>? = null
): Serializable