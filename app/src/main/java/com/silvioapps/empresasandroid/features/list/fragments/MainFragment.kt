package com.silvioapps.empresasandroid.features.list.fragments

import android.annotation.SuppressLint
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.DefaultItemAnimator
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.silvioapps.empresasandroid.R
import com.silvioapps.empresasandroid.databinding.FragmentMainBinding
import com.silvioapps.empresasandroid.features.list.adapters.MainListAdapter
import com.silvioapps.empresasandroid.features.list.models.EnterprisesItem
import com.silvioapps.empresasandroid.features.shared.fragments.CustomFragment
import com.silvioapps.empresasandroid.features.shared.listeners.FetcherListener
import dagger.android.support.AndroidSupportInjection
import java.io.Serializable
import javax.inject.Inject
import com.silvioapps.empresasandroid.features.list.view_models.ListViewModel
import com.silvioapps.empresasandroid.features.login.models.LoginResponse
import com.silvioapps.empresasandroid.features.shared.arc.Resource
import com.silvioapps.empresasandroid.features.shared.arc.Status
import com.silvioapps.empresasandroid.features.shared.utils.Utils

class MainFragment @Inject constructor(): CustomFragment() {
    @Inject lateinit var mainListAdapter: MainListAdapter
    private lateinit var fragmentMainBinding: FragmentMainBinding
    @Inject lateinit var context_: Context
    @Inject lateinit var linearLayoutManager: LinearLayoutManager
    @Inject lateinit var defaultItemAnimator: DefaultItemAnimator
    @Inject lateinit var fetcherListener: FetcherListener
    lateinit var loginResponse: LoginResponse
    private var searchViewExpanded = false
    private var searchViewQuery: String? = null
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    val listViewModel: ListViewModel by viewModels{
        viewModelFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(layoutInflater : LayoutInflater, viewGroup : ViewGroup?, bundle : Bundle?) : View?{
        fragmentMainBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_main, viewGroup, false)

        fragmentMainBinding.recyclerView.apply{
            layoutManager = linearLayoutManager
            itemAnimator = defaultItemAnimator
            setHasFixedSize(true)
            adapter = mainListAdapter
        }

        if(bundle != null){
            @Suppress("UNCHECKED_CAST")
            setEnterprisesItemList(bundle.getSerializable("enterprisesItemsList") as List<EnterprisesItem>)
            loginResponse = bundle.getSerializable("loginResponse") as LoginResponse
            searchViewExpanded = bundle.getBoolean("searchViewExpanded")
            searchViewQuery = bundle.getString("searchViewQuery")
        }
        else{
            fragmentMainBinding.tipTextView.visibility = View.VISIBLE
            loginResponse = arguments?.getSerializable("login") as LoginResponse
        }

        return fragmentMainBinding.root
    }

    override fun onAttach(context: Context){
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onSaveInstanceState(outState : Bundle) {
        outState.putSerializable("enterprisesItemsList", mainListAdapter.list as Serializable)
        outState.putSerializable("loginResponse", loginResponse as Serializable)
        outState.putBoolean("searchViewExpanded", searchViewExpanded)
        outState.putString("searchViewQuery", searchViewQuery)
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater)

        menuInflater.inflate(R.menu.menu_main, menu)

        val searchView = menu.findItem(R.id.action_search).getActionView() as SearchView
        if(searchViewExpanded){
            searchView.setIconifiedByDefault(false)
            searchView.setQuery(searchViewQuery, false)
        }
        searchView.setMaxWidth(Integer.MAX_VALUE)
        searchView.queryHint = getString(R.string.search_view_query_hint)
        Utils.setQueryHintColor(searchView, R.color.colorText)
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                search(loginResponse.accessToken!!, loginResponse.client!!, loginResponse.uid!!,
                    "%"+query+"%")
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                searchViewQuery = query
                searchViewExpanded = false
                if(query.length > 0) {
                    searchViewExpanded = true
                }
                return false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        if (id == R.id.action_search) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    protected fun search(accessToken: String, client: String, uid: String, search: String) {
        fetcherListener.beginFetching()
        fragmentMainBinding.progressBar.setVisibility(View.VISIBLE)

        listViewModel.getLiveDataResorceEnterprisesItemList(accessToken, client, uid, search)
            .observe(this, object: Observer<Resource<List<EnterprisesItem>>> {
                override fun onChanged(t: Resource<List<EnterprisesItem>>?) {
                    if (t?.status == Status.SUCCESS) {
                        setEnterprisesItemList(t.data!!)
                        fetcherListener.doneFetching()
                    }
                    else if (t?.status == Status.ERROR) {
                        fragmentMainBinding.progressBar.setVisibility(View.GONE)
                        Toast.makeText(context_, getString(R.string.list_error), Toast.LENGTH_LONG).show()
                        fetcherListener.doneFetching()
                    }
                    else if (t?.status == Status.LOADING) {
                        fetcherListener.beginFetching()
                    }
                }
            })
    }

    protected fun setEnterprisesItemList(values: List<EnterprisesItem>){
        mainListAdapter.list.clear()
        mainListAdapter.list.addAll(values)
        mainListAdapter.notifyDataSetChanged()

        fragmentMainBinding.progressBar.setVisibility(View.GONE)
        fragmentMainBinding.tipTextView.visibility = View.GONE
    }
}
