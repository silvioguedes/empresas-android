package com.silvioapps.empresasandroid.features.login.repositories

import com.silvioapps.empresasandroid.features.shared.arc.AppExecutors
import com.silvioapps.empresasandroid.features.login.daos.LoginDao
import com.silvioapps.empresasandroid.features.login.models.LoginResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoginRepository @Inject constructor(private val appExecutors: AppExecutors, val dao: LoginDao){

    fun setLoginResponse(value: LoginResponse){
        dao.setLoginResponse(value)
    }

    suspend fun getLoginResponse(): LoginResponse {
        return dao.getLoginResponse()
    }
}

