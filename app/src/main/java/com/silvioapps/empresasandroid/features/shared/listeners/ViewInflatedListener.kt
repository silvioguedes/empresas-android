package com.silvioapps.empresasandroid.features.shared.listeners

interface ViewInflatedListener {
    fun onInflated()
}
