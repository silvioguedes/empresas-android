package com.silvioapps.empresasandroid.features.list.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class EnterprisesItem(
	@PrimaryKey(autoGenerate = true)
	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("enterprise_type")
	val enterpriseType: EnterpriseType? = null,

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("facebook")
	val facebook: String? = null,

	@field:SerializedName("photo")
	val photo: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("linkedin")
	val linkedin: String? = null,

	@field:SerializedName("twitter")
	val twitter: String? = null,

	@field:SerializedName("email_enterprise")
	val emailEnterprise: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("own_enterprise")
	val ownEnterprise: Boolean? = null,

	@field:SerializedName("enterprise_name")
	val enterpriseName: String? = null,

	@field:SerializedName("value")
	val value: Int? = null,

	@field:SerializedName("share_price")
	val sharePrice: Double? = null
): Serializable