package com.silvioapps.empresasandroid.features.login.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.Volley
import com.google.gson.GsonBuilder
import com.silvioapps.empresasandroid.R
import com.silvioapps.empresasandroid.constants.Constants
import com.silvioapps.empresasandroid.databinding.FragmentLoginBinding
import com.silvioapps.empresasandroid.features.list.activities.MainActivity
import com.silvioapps.empresasandroid.features.list.view_models.LoginViewModel
import com.silvioapps.empresasandroid.features.login.models.LoginResponse
import com.silvioapps.empresasandroid.features.shared.api.HeaderRequest
import com.silvioapps.empresasandroid.features.shared.fragments.CustomFragment
import com.silvioapps.empresasandroid.features.shared.listeners.FetcherListener
import dagger.android.support.AndroidSupportInjection
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import javax.inject.Inject
import com.android.volley.Response as VolleyResponse

class LoginFragment @Inject constructor(): CustomFragment() {
    private lateinit var fragmentLoginBinding : FragmentLoginBinding
    @Inject lateinit var context_: Context
    @Inject lateinit var fetcherListener: FetcherListener
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    val loginViewModel: LoginViewModel by viewModels{
        viewModelFactory
    }

    override fun onCreateView(layoutInflater : LayoutInflater, viewGroup : ViewGroup?, bundle : Bundle?) : View? {
        fragmentLoginBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_login, viewGroup, false)
        fragmentLoginBinding.progressBar.visibility = View.GONE
        fragmentLoginBinding.enterButton.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                login(fragmentLoginBinding.emailEditText.text.toString(),
                    fragmentLoginBinding.passwordEditText.text.toString())
            }
        })

        return fragmentLoginBinding.root
    }

    override fun onAttach(context: Context){
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    fun login(email: String, password: String){
        fragmentLoginBinding.progressBar.visibility = View.VISIBLE

        try{
            val url = Constants.API_BASE_URL + Constants.LOGIN
            val jsonObject = JSONObject()
            jsonObject.put("email", email)
            jsonObject.put("password", password)

            val jsonObjectRequest = HeaderRequest(Request.Method.POST, url, jsonObject, object: VolleyResponse.Listener<JSONObject>{
                @SuppressLint("CheckResult")
                override fun onResponse(response: JSONObject?) {
                    fragmentLoginBinding.progressBar.visibility = View.GONE

                    val headers = response?.getJSONObject("headers")
                    val loginResponse: LoginResponse = GsonBuilder().create().fromJson(response.toString(), LoginResponse::class.java)

                    loginResponse.apply{
                        accessToken = headers?.get("access-token") as String
                        client = headers.get("client") as String
                        uid = headers.get("uid") as String
                    }

                    Observable.fromCallable({
                        loginViewModel.setLoginResponse(loginResponse)
                    }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            val intent = Intent(context_, MainActivity::class.java)
                            val bundle = Bundle()
                            bundle.putSerializable("login", loginResponse)
                            intent.putExtra("data", bundle)
                            activity?.startActivity(intent)
                            activity?.finish()
                        }
                }
            }, object: VolleyResponse.ErrorListener{
                override fun onErrorResponse(error: VolleyError?) {
                    fragmentLoginBinding.progressBar.visibility = View.GONE

                    Toast.makeText(context_, getString(R.string.list_error), Toast.LENGTH_LONG).show()
                }
            })

            Volley.newRequestQueue(context_).add(jsonObjectRequest)
        }
        catch(e: Exception){
            e.printStackTrace()
        }
    }
}
