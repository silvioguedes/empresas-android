package com.silvioapps.empresasandroid.features.list.repositories

import java.util.concurrent.TimeUnit
import androidx.lifecycle.LiveData
import com.silvioapps.empresasandroid.constants.Constants
import com.silvioapps.empresasandroid.features.shared.arc.AppExecutors
import com.silvioapps.empresasandroid.features.shared.arc.NetworkBoundResource
import com.silvioapps.empresasandroid.features.shared.arc.RateLimiter
import com.silvioapps.empresasandroid.features.shared.arc.Resource
import com.silvioapps.empresasandroid.features.list.daos.ListDao
import com.silvioapps.empresasandroid.features.list.models.ListResponse
import com.silvioapps.empresasandroid.features.list.models.EnterprisesItem
import com.silvioapps.empresasandroid.features.list.services.ListService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ListRepository @Inject constructor(private val appExecutors: AppExecutors, val dao: ListDao, val service: ListService){
    private val rateLimit = RateLimiter<String>(Constants.RATE_TIMEOUT, TimeUnit.MINUTES)

    fun getLiveDataResorceEnterprisesItemList(accessToken: String, client: String, uid: String, search: String): LiveData<Resource<List<EnterprisesItem>>> {
        return object : NetworkBoundResource<List<EnterprisesItem>, ListResponse>(appExecutors) {
            override fun saveCallResult(item: ListResponse) {
                dao.setEnterprisesItemList(item.enterprises!!)
            }

            override fun shouldFetch(data: List<EnterprisesItem>?): Boolean {
                return data == null || rateLimit.shouldFetch(null)
            }

            override fun loadFromDb(): LiveData<List<EnterprisesItem>> {
                return dao.getLiveDataEnterprisesItemList(search)
            }

            override fun createCall() = service.getLiveDataApiResponse(accessToken, client, uid, search)

            override fun onFetchFailed() {}
            
        }.asLiveData()
    }
}

