package com.silvioapps.empresasandroid.features.login.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.silvioapps.empresasandroid.features.login.fragments.LoginFragment
import com.silvioapps.empresasandroid.R
import com.silvioapps.empresasandroid.constants.Constants
import com.silvioapps.empresasandroid.features.list.activities.MainActivity
import com.silvioapps.empresasandroid.features.shared.views.activities.CustomActivity
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class LoginActivity : CustomActivity(), HasAndroidInjector {
    @Inject lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Any>
    @Inject lateinit var loginFragment: LoginFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if(savedInstanceState == null) {
            attachFragment(R.id.fragmentContainerView, loginFragment)
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return fragmentDispatchingAndroidInjector
    }
}