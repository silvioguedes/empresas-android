package com.silvioapps.empresasandroid.features.details.activities

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.silvioapps.empresasandroid.features.details.fragments.DetailsFragment
import com.silvioapps.empresasandroid.R
import com.silvioapps.empresasandroid.features.list.models.EnterprisesItem
import com.silvioapps.empresasandroid.features.shared.views.activities.CustomActivity
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class DetailsActivity : CustomActivity(), HasAndroidInjector {
    @Inject lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Any>
    @Inject lateinit var detailsFragment : DetailsFragment

    override fun onCreate(savedInstanceState : Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val enterprisesItem = intent.getBundleExtra("data").getSerializable("details") as EnterprisesItem
        showBackButton(findViewById(R.id.toolBar), enterprisesItem.enterpriseName!!)

        if(savedInstanceState == null) {
            detailsFragment.setArguments(intent.getBundleExtra("data"))
            attachFragment(R.id.fragmentContainerView, detailsFragment)
        }
    }

    override fun onSupportNavigateUp() : Boolean{
        onBackPressed()
        return true
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return fragmentDispatchingAndroidInjector
    }
}
