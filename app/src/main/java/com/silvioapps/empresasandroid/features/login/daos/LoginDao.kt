package com.silvioapps.empresasandroid.features.login.daos

import androidx.room.*
import com.silvioapps.empresasandroid.features.login.models.LoginResponse

@Dao
abstract class LoginDao{

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun setLoginResponse(response: LoginResponse)

    @Query("SELECT * FROM LoginResponse")
    abstract fun getLoginResponse(): LoginResponse
}