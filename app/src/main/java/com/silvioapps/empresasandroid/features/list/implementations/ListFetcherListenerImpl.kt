package com.silvioapps.empresasandroid.features.list.implementations

import com.silvioapps.empresasandroid.features.shared.listeners.FetcherListener
import javax.inject.Inject

class ListFetcherListenerImpl @Inject constructor(): FetcherListener {
    override fun beginFetching(){}
    override fun doneFetching(){}
    override fun isIdle(): Boolean{
        return true
    }
}