package com.silvioapps.empresasandroid.features.login.models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class LoginResponse(
	@PrimaryKey(autoGenerate = false)
	var id: Int = 1,

	@field:SerializedName("investor")
	@Embedded(prefix = "investor_")
	val investor: Investor? = null,

	//@field:SerializedName("enterprise")
	//val enterprise: Any? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	var accessToken: String? = null,
	var client: String? = null,
	var uid: String? = null
): Serializable