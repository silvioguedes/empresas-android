package com.silvioapps.empresasandroid.features.shared.listeners

interface ImageFetchListener {
    fun beginFetching()
    fun doneFetching()
    fun isIdle(): Boolean
    fun getPosition(): Int
    fun setPosition(position: Int)
}