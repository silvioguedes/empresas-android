package com.silvioapps.empresasandroid.features.details.fragments

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import com.silvioapps.empresasandroid.R
import com.silvioapps.empresasandroid.databinding.FragmentDetailsBinding
import com.silvioapps.empresasandroid.features.details.implementations.DetailsPicassoCallbackListener
import com.silvioapps.empresasandroid.features.list.models.EnterprisesItem
import com.silvioapps.empresasandroid.features.shared.fragments.CustomFragment
import com.silvioapps.empresasandroid.features.shared.utils.Utils
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class DetailsFragment @Inject constructor(): CustomFragment() {
    private lateinit var fragmentDetailsBinding : FragmentDetailsBinding
    @Inject lateinit var detailsPicassoCallbackListener: DetailsPicassoCallbackListener

    override fun onCreateView(layoutInflater : LayoutInflater, viewGroup : ViewGroup?, bundle : Bundle?) : View? {
        fragmentDetailsBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_details, viewGroup, false)

        val enterprisesItem = arguments?.getSerializable("details") as EnterprisesItem
        fragmentDetailsBinding.enterprisesItem = enterprisesItem
        fragmentDetailsBinding.callback = detailsPicassoCallbackListener
        fragmentDetailsBinding.imageView.apply{
            layoutParams.height = (Utils.getScreenSize(context).y / ResourcesCompat.getFloat(context.resources, R.dimen.details_size_percent)).toInt()
        }

        return fragmentDetailsBinding.root
    }

    override fun onAttach(context: Context){
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }
}
