package com.silvioapps.empresasandroid.features.shared.api

import com.android.volley.NetworkResponse
import com.android.volley.ParseError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException

class HeaderRequest constructor(method: Int, url: String, jsonRequest: JSONObject,
                                listener: Response.Listener<JSONObject>, errorListener: Response.ErrorListener):
    JsonObjectRequest(method, url, jsonRequest, listener, errorListener) {

    constructor(url: String, jsonRequest: JSONObject, listener: Response.Listener<JSONObject>,
                errorListener: Response.ErrorListener): this(0, url, jsonRequest, listener, errorListener) {
    }

    override fun parseNetworkResponse(response: NetworkResponse): Response<JSONObject> {
        try {
            val jsonString = String(response.data, charset(HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET)))
            val jsonResponse = JSONObject(jsonString)
            jsonResponse.put("headers", JSONObject(response.headers))
            return Response.success(jsonResponse, HttpHeaderParser.parseCacheHeaders(response))
        } catch (e: UnsupportedEncodingException) {
            return Response.error(ParseError(e))
        } catch (je: JSONException) {
            return Response.error(ParseError(je))
        }
    }
}

