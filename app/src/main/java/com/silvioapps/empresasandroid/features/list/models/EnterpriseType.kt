package com.silvioapps.empresasandroid.features.list.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class EnterpriseType(
	@PrimaryKey(autoGenerate = true)
	@field:SerializedName("id")
	var id: Int? = null,

	@field:SerializedName("enterprise_type_name")
	val enterpriseTypeName: String? = null
): Serializable