package com.silvioapps.empresasandroid.features.login.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
data class Investor(
	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("balance")
	val balance: Double? = null,

	@field:SerializedName("portfolio_value")
	val portfolioValue: Double? = null,

	@field:SerializedName("first_access")
	val firstAccess: Boolean? = null,

	@field:SerializedName("investor_name")
	val investorName: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("portfolio")
	val portfolio: Portfolio? = null,

	//@field:SerializedName("photo")
	//val photo: Any? = null,

	@field:SerializedName("super_angel")
	val superAngel: Boolean? = null,

	@field:SerializedName("email")
	val email: String? = null
): Serializable