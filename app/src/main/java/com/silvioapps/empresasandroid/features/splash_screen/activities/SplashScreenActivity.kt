package com.silvioapps.empresasandroid.features.splash_screen.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.silvioapps.empresasandroid.features.login.activities.LoginActivity

class SplashScreenActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
}
