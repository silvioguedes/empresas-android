package com.silvioapps.empresasandroid.features.list.adapters

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.silvioapps.empresasandroid.BR
import com.silvioapps.empresasandroid.R
import com.silvioapps.empresasandroid.features.list.implementations.ListViewClickListener
import com.silvioapps.empresasandroid.features.list.models.EnterprisesItem
import com.silvioapps.empresasandroid.features.shared.listeners.PicassoCallbackListener
import com.silvioapps.empresasandroid.features.shared.utils.Utils
import com.silvioapps.empresasandroid.features.shared.listeners.ViewClickListener

class MainListAdapter (val list : MutableList<EnterprisesItem>, viewClickListener_ : ListViewClickListener, callback_: PicassoCallbackListener) : RecyclerView.Adapter<MainListAdapter.BindingViewHolder>() {
    companion object{
        private lateinit var viewClickListener: ViewClickListener
        private lateinit var callback: PicassoCallbackListener
    }

    init{
        viewClickListener = viewClickListener_
        callback = callback_
    }

    class BindingViewHolder(view : View, list: MutableList<EnterprisesItem>) : RecyclerView.ViewHolder(view){
        var viewDataBinding : ViewDataBinding? = null

        init{
            viewDataBinding = DataBindingUtil.bind<ViewDataBinding>(view)
            Utils.setClickListeners(view, viewClickListener, list)
        }
    }

    override fun onCreateViewHolder(parent : ViewGroup, viewType: Int) : BindingViewHolder{
        val view = LayoutInflater.from(parent.context).inflate(R.layout.main_list_layout, parent, false)
        return BindingViewHolder(view, list)
    }

    override fun onBindViewHolder(holder : BindingViewHolder, position : Int) {
        Utils.setTags(position, holder.itemView)

        if(list.size > position) {
            val enterprisesItem = list.get(position)
            holder.viewDataBinding?.setVariable(BR.callback, callback)
            holder.viewDataBinding?.setVariable(BR.enterprisesItem, enterprisesItem)
            holder.viewDataBinding?.setVariable(BR.position, position)
            holder.viewDataBinding?.executePendingBindings()
        }
    }

    override fun getItemCount() : Int{
        return list.size
    }
}
