package com.silvioapps.empresasandroid.features.shared.listeners

interface FetcherListener {
    fun beginFetching()
    fun doneFetching()
    fun isIdle(): Boolean
}