package com.silvioapps.empresasandroid.features.list.view_models

import androidx.lifecycle.ViewModel
import com.silvioapps.empresasandroid.features.login.repositories.LoginRepository
import com.silvioapps.empresasandroid.features.login.models.LoginResponse
import javax.inject.Inject;

class LoginViewModel @Inject constructor(private var repository: LoginRepository): ViewModel() {

    suspend fun getLoginResponse(): LoginResponse {
        return repository.getLoginResponse()
    }

    fun setLoginResponse(value: LoginResponse){
        return repository.setLoginResponse(value)
    }
}