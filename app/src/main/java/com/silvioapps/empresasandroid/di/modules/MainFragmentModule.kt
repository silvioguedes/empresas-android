package com.silvioapps.empresasandroid.di.modules

import com.silvioapps.empresasandroid.features.list.adapters.MainListAdapter
import com.silvioapps.empresasandroid.features.list.implementations.*
import com.silvioapps.empresasandroid.features.list.models.EnterprisesItem
import dagger.Module
import dagger.Provides

@Module
open class MainFragmentModule{

    @Provides
    open fun providesMutableListOfResponseItem(): MutableList<EnterprisesItem>{
        return mutableListOf<EnterprisesItem>()
    }

    @Provides
    open fun providesMainListAdapter(list: MutableList<EnterprisesItem>, viewClickListener : ListViewClickListener, callback: ListPicassoCallbackListener): MainListAdapter {
        return MainListAdapter(list, viewClickListener, callback)
    }

    @Provides
    open fun providesListViewClickListener(): ListViewClickListener{
        return ListViewClickListenerImpl()
    }

    @Provides
    open fun providesListPicassoCallbackListener(imageFetchListener: ListImageFetchListener): ListPicassoCallbackListener {
        return ListPicassoCallbackListenerImpl(imageFetchListener)
    }
}