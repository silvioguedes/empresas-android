package com.silvioapps.empresasandroid.di.modules

import com.silvioapps.empresasandroid.features.details.activities.DetailsActivity
import com.silvioapps.empresasandroid.features.list.activities.MainActivity
import com.silvioapps.empresasandroid.features.login.activities.LoginActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {
    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributesDetailsActivity(): DetailsActivity

    @ContributesAndroidInjector
    abstract fun contributesLoginActivity(): LoginActivity
}
