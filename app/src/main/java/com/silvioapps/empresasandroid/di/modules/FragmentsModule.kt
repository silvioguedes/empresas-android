package com.silvioapps.empresasandroid.di.modules

import com.silvioapps.empresasandroid.features.details.fragments.DetailsFragment
import com.silvioapps.empresasandroid.features.list.fragments.MainFragment
import com.silvioapps.empresasandroid.features.login.fragments.LoginFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {
    @ContributesAndroidInjector
    abstract fun contributesMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributesDetailsFragment(): DetailsFragment

    @ContributesAndroidInjector
    abstract fun contributesLoginFragment(): LoginFragment
}
