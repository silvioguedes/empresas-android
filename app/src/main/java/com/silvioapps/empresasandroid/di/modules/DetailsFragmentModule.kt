package com.silvioapps.empresasandroid.di.modules

import com.silvioapps.empresasandroid.features.details.implementations.DetailsImageFetchListener
import com.silvioapps.empresasandroid.features.details.implementations.DetailsPicassoCallbackListener
import com.silvioapps.empresasandroid.features.details.implementations.DetailsPicassoCallbackListenerImpl
import dagger.Module
import dagger.Provides

@Module
open class DetailsFragmentModule{

    @Provides
    open fun providesDetailsPicassoCallbackListener(imageFetchListener: DetailsImageFetchListener): DetailsPicassoCallbackListener {
        return DetailsPicassoCallbackListenerImpl(imageFetchListener)
    }
}