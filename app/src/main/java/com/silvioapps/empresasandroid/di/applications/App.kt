package com.silvioapps.empresasandroid.di.applications

import android.app.Application
import com.silvioapps.empresasandroid.di.components.DaggerAppComponent
import com.silvioapps.empresasandroid.di.modules.*
import com.silvioapps.empresasandroid.features.shared.utils.Utils
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

open class App : Application(), HasAndroidInjector{
    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

        setComponent()

        Utils.fixSSLError(getApplicationContext())
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }

    open fun setComponent(){
        DaggerAppComponent
            .builder()
            .application(this)
            .recyclerViewModule(RecyclerViewModule())
            .mainFragmentModule(MainFragmentModule())
            .apiModule(ApiModule())
            .databaseModule(DatabaseModule())
            .fetcherModule(FetcherModule())
            .detailsFragmentModule(DetailsFragmentModule())
            .imageFetchModule(ImageFetchModule())
            .build()
            .inject(this)
    }
}
