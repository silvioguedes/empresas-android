package com.silvioapps.empresasandroid.di.modules

import com.silvioapps.empresasandroid.constants.Constants
import com.silvioapps.empresasandroid.features.list.services.ListService
import com.silvioapps.empresasandroid.features.shared.services.ServiceGenerator
import dagger.Module
import dagger.Provides

@Module
open class ApiModule{

    @Provides
    open fun providesListService(): ListService {
        return ServiceGenerator.createService(Constants.API_BASE_URL, Constants.API_TIMEOUT, true, ListService::class.java)
    }
}