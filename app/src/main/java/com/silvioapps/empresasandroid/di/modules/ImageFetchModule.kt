package com.silvioapps.empresasandroid.di.modules

import com.silvioapps.empresasandroid.features.details.implementations.DetailsImageFetchListener
import com.silvioapps.empresasandroid.features.details.implementations.DetailsImageFetchListenerImpl
import com.silvioapps.empresasandroid.features.list.implementations.ListImageFetchListener
import com.silvioapps.empresasandroid.features.list.implementations.ListImageFetchListenerImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class ImageFetchModule {

    @Provides
    @Singleton
    open fun providesListImageFetchListener(): ListImageFetchListener {
        return ListImageFetchListenerImpl()
    }

    @Provides
    @Singleton
    open fun providesDetailsImageFetchListener(): DetailsImageFetchListener {
        return DetailsImageFetchListenerImpl()
    }
}