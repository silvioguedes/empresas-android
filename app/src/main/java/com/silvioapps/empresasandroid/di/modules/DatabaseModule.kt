package com.silvioapps.empresasandroid.di.modules

import android.app.Application
import androidx.room.Room
import com.silvioapps.empresasandroid.features.list.daos.ListDao
import com.silvioapps.empresasandroid.features.login.daos.LoginDao
import com.silvioapps.empresasandroid.db.databases.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class DatabaseModule{

    @Singleton
    @Provides
    open fun providesAppDatabase(application: Application): AppDatabase {
        return Room
            .databaseBuilder(application, AppDatabase::class.java, "database.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    open fun providesListDao(db: AppDatabase): ListDao {
        return db.listDao()
    }

    @Singleton
    @Provides
    open fun providesLoginDao(db: AppDatabase): LoginDao {
        return db.loginDao()
    }
}

