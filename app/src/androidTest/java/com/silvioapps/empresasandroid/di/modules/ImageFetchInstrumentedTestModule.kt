package com.silvioapps.empresasandroid.di.modules

import com.silvioapps.empresasandroid.features.details.implementations.DetailsImageFetchListener
import com.silvioapps.empresasandroid.features.list.implementations.ListImageFetchListener
import com.silvioapps.empresasandroid.features.details.implementations.DetailsImageFetchListenerInstrumentedTestImpl
import com.silvioapps.empresasandroid.features.list.implementations.ListImageFetchListenerInstrumentedTestImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ImageFetchInstrumentedTestModule: ImageFetchModule() {

    @Provides
    @Singleton
    override fun providesListImageFetchListener(): ListImageFetchListener {
        return ListImageFetchListenerInstrumentedTestImpl.getInstance()!!
    }

    @Provides
    @Singleton
    override fun providesDetailsImageFetchListener(): DetailsImageFetchListener {
        return DetailsImageFetchListenerInstrumentedTestImpl.getInstance()!!
    }
}