package com.silvioapps.empresasandroid.di.modules

import com.silvioapps.empresasandroid.features.shared.implementations.FetcherListenerInstrumentedTestImpl
import com.silvioapps.empresasandroid.features.shared.listeners.FetcherListener
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FetcherInstrumentedTestModule: FetcherModule() {

    @Provides
    @Singleton
    override fun providesFetcherListener(): FetcherListener {
        return FetcherListenerInstrumentedTestImpl.getInstance()!!
    }
}