package com.silvioapps.empresasandroid.features.list

import android.content.Intent
import android.os.Bundle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.silvioapps.empresasandroid.R
import org.junit.Before
import com.silvioapps.empresasandroid.features.details.activities.DetailsActivity
import com.silvioapps.empresasandroid.features.list.models.EnterprisesItem
import com.silvioapps.empresasandroid.features.shared.matchers.CustomMatchers.Companion.backButton
import org.hamcrest.Matchers.*
import java.io.Serializable

@RunWith(AndroidJUnit4::class)
@LargeTest
class DetailsFragmentMockInstrumentedTest {
    lateinit var activityScenario: ActivityScenario<DetailsActivity>

    @Before
    fun before(){
        val intent = Intent(InstrumentationRegistry.getInstrumentation().targetContext, DetailsActivity::class.java)
        val bundle = Bundle()

        val enterprisesItem = EnterprisesItem(
            country = "Brasil",
            photo = "null",
            description = "Teste",
            enterpriseName = "Teste"
        )

        bundle.putSerializable("details", enterprisesItem as Serializable)
        intent.putExtra("data", bundle)

        activityScenario = launchActivity(intent)
    }

    @Test
    fun test(){
        onView(withId(R.id.imageView)).check(matches(isDisplayed()))

        onView(withId(R.id.enterpriseDescriptionsTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.enterpriseDescriptionsTextView)).check(matches(withText(startsWith("T"))))

        onView(withId(R.id.fragmentDetails)).check(matches(
            hasDescendant(allOf(withId(R.id.scrollView))))).perform(ViewActions.swipeUp())

        onView(backButton(R.id.toolBar)).perform(click())
    }
}
