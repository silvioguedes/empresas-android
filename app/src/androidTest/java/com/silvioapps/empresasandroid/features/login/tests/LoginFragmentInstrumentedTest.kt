package com.silvioapps.empresasandroid.features.list

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.silvioapps.empresasandroid.R
import org.junit.Rule
import com.silvioapps.empresasandroid.features.login.activities.LoginActivity

@RunWith(AndroidJUnit4::class)
@LargeTest
class LoginFragmentInstrumentedTest {
    @get:Rule val activityScenarioRule = activityScenarioRule<LoginActivity>()

    @Test
    fun test(){
        onView(withId(R.id.imageView)).check(matches(isDisplayed()))

        onView(withId(R.id.titleTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.titleTextView)).check(matches(withText(R.string.title_login)))

        onView(withId(R.id.subtitleTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.subtitleTextView)).check(matches(withText(R.string.subtitle_login)))

        onView(withId(R.id.emailEditText)).check(matches(isDisplayed()))

        onView(withId(R.id.passwordEditText)).check(matches(isDisplayed()))

        onView(withId(R.id.emailEditText)).perform(typeText("testeapple@ioasys.com.br"))

        onView(withId(R.id.passwordEditText)).perform(typeText("12341234"))

        onView(withId(R.id.enterButton)).check(matches(isDisplayed()))

        onView(withId(R.id.scrollView)).perform(swipeUp())

        onView(withId(R.id.enterButton)).perform(click())
    }
}
