package com.silvioapps.empresasandroid.features.list

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.silvioapps.empresasandroid.R
import org.junit.Rule
import com.silvioapps.empresasandroid.features.login.activities.LoginActivity

@RunWith(AndroidJUnit4::class)
@LargeTest
class LoginActivityInstrumentedTest {
    @get:Rule val activityScenarioRule = activityScenarioRule<LoginActivity>()

    @Test
    fun test(){
        onView(withId(R.id.fragmentContainerView)).check(matches(isDisplayed()))
    }
}
