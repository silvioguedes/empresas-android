package com.silvioapps.empresasandroid.features.login.async_tasks

import android.content.Context
import android.os.AsyncTask
import com.android.volley.Request
import com.android.volley.toolbox.RequestFuture
import com.android.volley.toolbox.Volley
import com.google.gson.GsonBuilder
import com.silvioapps.empresasandroid.constants.Constants
import com.silvioapps.empresasandroid.features.login.models.LoginResponse
import com.silvioapps.empresasandroid.features.shared.api.HeaderRequest
import org.json.JSONObject
import java.lang.ref.WeakReference

class LoginAsyncTask(context: Context): AsyncTask<Void, Void, LoginResponse>() {
        var weakRefereceContext: WeakReference<Context>

        init{
            weakRefereceContext = WeakReference(context)
        }

        override fun onPreExecute() {}

        override fun doInBackground(vararg p0: Void): LoginResponse? {
            val url = Constants.API_BASE_URL + Constants.LOGIN
            val jsonObject = JSONObject()
            jsonObject.put("email", "testeapple@ioasys.com.br")
            jsonObject.put("password", "12341234")

            val requestFuture = RequestFuture.newFuture<JSONObject>()
            val jsonObjectRequest = HeaderRequest(Request.Method.POST, url, jsonObject, requestFuture, requestFuture)

            Volley.newRequestQueue(weakRefereceContext.get()).add(jsonObjectRequest)

            var loginResponse: LoginResponse? = null
            try{
                val response = requestFuture.get()
                val headers = response?.getJSONObject("headers")

                loginResponse = GsonBuilder().create().fromJson(response.toString(), LoginResponse::class.java)
                loginResponse?.apply{
                    accessToken = headers?.get("access-token") as String
                    client = headers.get("client") as String
                    uid = headers.get("uid") as String
                }
            }
            catch(e: Exception){
                e.printStackTrace()
            }

            return loginResponse
        }

        override fun onProgressUpdate(vararg p0: Void) {}

        override fun onPostExecute(result: LoginResponse) {}
    }