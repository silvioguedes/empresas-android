package com.silvioapps.empresasandroid.features.list.tests

import android.content.Intent
import android.os.Bundle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.silvioapps.empresasandroid.R
import com.silvioapps.empresasandroid.di.applications.AppInstrumentedTest
import com.silvioapps.empresasandroid.features.list.activities.MainActivity
import com.silvioapps.empresasandroid.features.login.async_tasks.LoginAsyncTask
import com.silvioapps.empresasandroid.features.shared.actions.CustomActions.Companion.submitText
import com.silvioapps.empresasandroid.features.shared.async_tasks.FetcherAsyncTask
import com.silvioapps.empresasandroid.features.shared.implementations.FetcherListenerInstrumentedTestImpl
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityInstrumentedTest {
    lateinit var activityScenario: ActivityScenario<MainActivity>

    @Before
    fun before(){
        val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as AppInstrumentedTest
       app.setComponent()

        val loginResponse = LoginAsyncTask(app).execute().get()

        val intent = Intent(app, MainActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("login",loginResponse)
        intent.putExtra("data", bundle)

        activityScenario = launchActivity(intent)
    }

    @After
    fun after(){
        FetcherListenerInstrumentedTestImpl.destroyInstance()
    }

    @Test
    fun test() {
        onView(withId(R.id.fragmentContainerView)).check(matches(isDisplayed()))

        onView(withId(R.id.toolBar)).check(matches(isDisplayed()))

        onView(withId(R.id.toolBar)).check(matches(
            hasDescendant(allOf(withId(R.id.imageView), isDisplayed()))))

        onView(withId(R.id.action_search)).check(matches(isDisplayed()))

        onView(withId(R.id.action_search)).perform(click())

        onView(withId(R.id.action_search)).perform(submitText("teste"))

        FetcherAsyncTask(FetcherListenerInstrumentedTestImpl.getInstance()!!).execute().get()

        onView(withId(R.id.toolBar)).check(matches(
            hasDescendant(allOf(withId(R.id.imageView), not(isDisplayed())))))
    }
}
