package com.silvioapps.empresasandroid.features.list.tests

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.silvioapps.empresasandroid.R
import com.silvioapps.empresasandroid.di.applications.AppInstrumentedTest
import com.silvioapps.empresasandroid.features.list.activities.MainActivity
import com.silvioapps.empresasandroid.features.login.async_tasks.LoginAsyncTask
import com.silvioapps.empresasandroid.features.shared.actions.CustomActions.Companion.submitText
import org.junit.Before
import com.silvioapps.empresasandroid.features.shared.async_tasks.FetcherAsyncTask
import com.silvioapps.empresasandroid.features.list.async_tasks.ListImageFetchAsyncTask
import com.silvioapps.empresasandroid.features.shared.implementations.FetcherListenerInstrumentedTestImpl
import com.silvioapps.empresasandroid.features.list.implementations.ListImageFetchListenerInstrumentedTestImpl
import com.silvioapps.empresasandroid.features.shared.matchers.CustomMatchers.Companion.atPosition
import org.hamcrest.Matchers.*
import org.junit.After

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainFragmentInstrumentedTest {
    lateinit var activityScenario: ActivityScenario<MainActivity>

    @Before
    fun before(){
        val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as AppInstrumentedTest
        app.setComponent()

        val loginResponse = LoginAsyncTask(app).execute().get()

        val intent = Intent(app, MainActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("login",loginResponse)
        intent.putExtra("data", bundle)

        activityScenario = launchActivity(intent)
    }

    @After
    fun after(){
        ListImageFetchListenerInstrumentedTestImpl.destroyInstance()
        FetcherListenerInstrumentedTestImpl.destroyInstance()
    }

    @Test
    fun test(){
        onView(withId(R.id.tipTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.action_search)).perform(click())

        onView(withId(R.id.action_search)).perform(submitText("a"))

        FetcherAsyncTask(FetcherListenerInstrumentedTestImpl.getInstance()!!).execute().get()

        onView(withId(R.id.recyclerView)).check(matches(isDisplayed()))

        var position = 0

        val imageFetchListenerInstrumentedTestImpl = ListImageFetchListenerInstrumentedTestImpl.getInstance()
        imageFetchListenerInstrumentedTestImpl?.setPosition(position)
        ListImageFetchAsyncTask(
            imageFetchListenerInstrumentedTestImpl!!
        ).execute().get()

        onView(withId(R.id.recyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.imageView), isDisplayed())))))

        onView(withId(R.id.recyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.enterpriseNameTextView), withText("AllRide"))))))

        onView(withId(R.id.recyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.enterpriseTypeTextView), withText(containsString("w")))))))

        onView(withId(R.id.recyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.countryTextView), withText(startsWith("C")))))))

        activityScenario.onActivity {
            position = it.mainFragment.mainListAdapter.itemCount - 1
        }

        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(position))

        imageFetchListenerInstrumentedTestImpl.setPosition(position)
        ListImageFetchAsyncTask(
            imageFetchListenerInstrumentedTestImpl
        ).execute().get()

        onView(withId(R.id.recyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.imageView), isDisplayed())))))

        onView(withId(R.id.recyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.enterpriseNameTextView), withText("VACuCh"))))))

        onView(withId(R.id.recyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.enterpriseTypeTextView), withText(containsString("P")))))))

        onView(withId(R.id.recyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.countryTextView), withText(endsWith("e")))))))

        onView(withId(R.id.tipTextView)).check(matches(not(isDisplayed())))

        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(position, click()))
    }
}
