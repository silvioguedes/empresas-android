package com.silvioapps.empresasandroid.features.shared.runners

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import com.silvioapps.empresasandroid.di.applications.AppInstrumentedTest

class TestRunner: AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        return super.newApplication(cl, AppInstrumentedTest::class.java.name, context)
    }
}