package com.silvioapps.empresasandroid.features.list

import android.content.Intent
import android.os.Bundle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.silvioapps.empresasandroid.R
import com.silvioapps.empresasandroid.di.applications.AppInstrumentedTest
import com.silvioapps.empresasandroid.features.details.activities.DetailsActivity
import com.silvioapps.empresasandroid.features.list.models.EnterprisesItem
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.endsWith
import org.junit.Before
import java.io.Serializable

@RunWith(AndroidJUnit4::class)
@LargeTest
class DetailsActivityInstrumentedTest {
    lateinit var activityScenario: ActivityScenario<DetailsActivity>

    @Before
    fun before(){
        val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as AppInstrumentedTest
       app.setComponent()

        val intent = Intent(app, DetailsActivity::class.java)
        val bundle = Bundle()

        val enterprisesItem = EnterprisesItem(
            country = "Brasil",
            photo = "null",
            description = "Teste",
            enterpriseName = "Teste"
        )

        bundle.putSerializable("details", enterprisesItem as Serializable)
        intent.putExtra("data", bundle)

        activityScenario = launchActivity(intent)
    }

    @Test
    fun test(){
        onView(withId(R.id.fragmentContainerView)).check(matches(isDisplayed()))

        onView(withId(R.id.toolBar)).check(matches(isDisplayed()))

        onView(withId(R.id.activityDetails)).check(matches(
            hasDescendant(allOf(withId(R.id.toolBar), hasDescendant(withText(endsWith("e")))))))

    }
}
